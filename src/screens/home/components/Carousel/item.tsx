/* eslint-disable react-native/no-inline-styles */
import IconHeart from '@components/Icons/icon-heart';
import ImageEvent from '@components/Images/image-event';
import Text from '@components/Text/text';
import TextDate from '@components/Text/text-date';
import {useAppDispatch} from '@hooks/store.hook';
import {Event} from '@models/events/eventResponse.model';
import {setFavoriteEvent} from '@store/slices/events.reducer';
import {colors} from '@theme/colors';
import {AnimationOpacity} from '@utilities/animations';
import {hp} from '@utilities/responsive';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Animated} from 'react-native';

type ItemCarouselProps = {
  item: Event;
  activeIndex: number;
  index: number;
};

const ItemCarousel = ({item, index, activeIndex}: ItemCarouselProps) => {
  const dispatch = useAppDispatch();
  const [fadeAnim] = useState(new Animated.Value(0));

  const handleFavorite = () => {
    dispatch(setFavoriteEvent(item));
  };

  useEffect(() => {
    AnimationOpacity(fadeAnim, activeIndex, index);
  }, [activeIndex, index, fadeAnim]);

  return (
    <View style={styles.item}>
      <ImageEvent item={item} />
      {activeIndex === index && (
        <Animated.View
          style={[styles.containerDescription, {opacity: fadeAnim}]}>
          <View style={{flex: 0.8}}>
            <TextDate date={item.end_date || item.end_at} />

            <Text category="p3" numberOfLines={1} style={{marginTop: 5}}>
              {item.title}
            </Text>
            <Text
              category="b5"
              style={{color: colors('redLight'), marginTop: 5}}>
              {item.location}
            </Text>
          </View>

          <IconHeart favorite={item.favorite} handleFavorite={handleFavorite} />
        </Animated.View>
      )}
    </View>
  );
};

export default ItemCarousel;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerDescription: {
    marginTop: hp(1),
    flexDirection: 'row',
    alignItems: 'center',
  },
  item: {
    backgroundColor: colors('whiteNormal'),
    alignItems: 'center',
  },
});
