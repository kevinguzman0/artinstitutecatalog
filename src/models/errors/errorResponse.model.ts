export interface ErrorFromApi {
  status: number;
  data: DataError;
}

interface DataError {
  error: string;
  status: number;
}
