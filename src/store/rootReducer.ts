import {combineReducers} from '@reduxjs/toolkit';

import {
  reducer as eventReducerApi,
  reducerPath as eventReducePath,
} from './apis/events.api';
import eventSliceReducer from './slices/events.reducer';

export type RootState = ReturnType<typeof combineReducers>;

export default combineReducers({
  [eventReducePath]: eventReducerApi, //(RTK Query)
  eventSliceReducer: eventSliceReducer, //(Redux Toolkit)
});
