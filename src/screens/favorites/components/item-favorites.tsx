/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {colors} from '@theme/colors';
import {hp, wp} from '@utilities/responsive';
import {StyleSheet, View} from 'react-native';
import Text from '@components/Text/text';
import {Event} from '@models/events/eventResponse.model';
import {setFavoriteEvent} from '@store/slices/events.reducer';
import IconHeart from '@components/Icons/icon-heart';
import ImageEvent from '@components/Images/image-event';
import TextDate from '@components/Text/text-date';
import {useAppDispatch} from '@hooks/store.hook';

type ItemProps = {
  item: Event;
};

const Item = ({item}: ItemProps) => {
  const dispatch = useAppDispatch();

  const handleFavorite = () => {
    dispatch(setFavoriteEvent(item));
  };

  return (
    <View>
      <ImageEvent item={item} />

      <View style={styles.containerDescription}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 0.8, marginBottom: hp(1)}}>
            <TextDate date={item.end_date || item.end_at} />

            <Text
              category="b5"
              style={{color: colors('redLight'), marginTop: 5}}>
              {item.location}
            </Text>
          </View>

          <IconHeart favorite={item.favorite} handleFavorite={handleFavorite} />
        </View>

        <Text category="p3" style={{marginTop: 5}}>
          {item.title}
        </Text>
      </View>

      <View style={{alignItems: 'center'}}>
        <View style={styles.divider} />
      </View>
    </View>
  );
};

export default Item;

const styles = StyleSheet.create({
  containerDescription: {
    marginTop: hp(1),
    paddingHorizontal: 10,
  },
  containerIcon: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  divider: {
    width: wp(85),
    borderBottomWidth: 0.5,
    borderBottomColor: colors('blackNormal'),
    marginTop: hp(2),
  },
});
