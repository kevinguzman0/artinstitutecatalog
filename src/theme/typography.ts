import {scaleFont} from '@utilities/fonts';
import {parseLiterals} from './string';

export const fontSizes = {
  fontSize10: scaleFont(10),
  fontSize13: scaleFont(13),
  fontSize16: scaleFont(16),
  fontSize18: scaleFont(18),
  fontSize24: scaleFont(24),
  fontSize31: scaleFont(31),
  fontSize39: scaleFont(39),
} as const;

/**
 * Tagged string function that return a defined font size from the scale by using it name. If no font size is defined will throw an error
 * @param literals Tagged string with one of the font sizes defined. Should come using the format fontSizeNumber or only the number. Ex.: fontSize24 or 24
 * @returns
 */
export function fontSize(literals: TemplateStringsArray) {
  const name = `fontSize${parseLiterals(literals)}`;
  if (!(name in fontSizes)) {
    throw new Error(
      `Requested font size ${name} not found. You can compose it with scaleFont helper`,
    );
  }
  return fontSizes[name as keyof typeof fontSizes];
}
