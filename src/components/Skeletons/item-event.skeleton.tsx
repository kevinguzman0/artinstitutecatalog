import React from 'react';
import {hp, wp} from '@utilities/responsive';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';
import {View} from 'react-native';

const ItemEventSkeleton = () => (
  <View style={{marginBottom: hp(4)}}>
    <ContentLoader
      speed={2}
      width={wp(100)}
      height={260}
      viewBox="0 0 450 260"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb">
      <Rect x="0" y="0" rx="19" ry="19" width="400" height="200" />

      <Rect x="10" y="210" rx="0" ry="0" width="300" height="8" />
      <Rect x="10" y="230" rx="0" ry="0" width="300" height="8" />
      <Rect x="10" y="250" rx="0" ry="0" width="300" height="8" />

      <Circle cx="350" cy="235" r="25" />
    </ContentLoader>
  </View>
);

export default ItemEventSkeleton;
