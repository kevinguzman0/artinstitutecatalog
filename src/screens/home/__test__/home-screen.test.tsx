import React from 'react';
import {render, fireEvent, waitFor} from '@testing-library/react-native';
import {Provider} from 'react-redux';
import HomeScreen from '@screens/home/home.screen';
import {useQueryEvents} from '@hooks/useQueryEvents.hook';
import store from '@store/index';

// Mock the hooks
jest.mock('@hooks/useQueryEvents.hook');

describe('HomeScreen', () => {
  beforeEach(() => {
    // Setup mock values
    (useQueryEvents as jest.Mock).mockReturnValue({
      isLoading: false,
      isFetching: false,
      refetchEventOccurrences: jest.fn(),
      refetchEvents: jest.fn(),
      mockReturnValue: jest.fn(),
    });
  });

  it('should render correctly', () => {
    const {getByText} = render(
      <Provider store={store}>
        <HomeScreen />
      </Provider>,
    );

    expect(getByText('Explore')).toBeTruthy();
  });

  it('should call refetchEvents and refetchEventOccurrences when refreshing', async () => {
    const refetchEvents = jest.fn();
    const refetchEventOccurrences = jest.fn();

    (useQueryEvents as jest.Mock).mockReturnValue({
      isLoading: false,
      isFetching: false,
      refetchEventOccurrences,
      refetchEvents,
    });

    const {getByTestId} = render(
      <Provider store={store}>
        <HomeScreen />
      </Provider>,
    );

    fireEvent(getByTestId('ScrollViewContainer'), 'onRefresh');

    await waitFor(() => {
      expect(refetchEvents).toHaveBeenCalled();
      expect(refetchEventOccurrences).toHaveBeenCalled();
    });
  });
});
