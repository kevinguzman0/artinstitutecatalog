import {
  Event,
  EventResponse,
  Pagination,
} from '@models/events/eventResponse.model';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type InitStateProps = {
  events: Event[];
  occurrences: Event[];
  pagination: Pagination;
  favorites: Event[];
};

const initialState: InitStateProps = {
  events: [] as Event[],
  occurrences: [] as Event[],
  favorites: [] as Event[],

  pagination: {} as Pagination,
};

const eventSlice = createSlice({
  name: 'event',
  initialState,
  reducers: {
    setEvents: (state, action: PayloadAction<EventResponse>) => {
      state.events = action.payload.data;
      state.pagination = action.payload.pagination;
      state.favorites = [];
    },
    setEventsOccurrences: (state, action: PayloadAction<EventResponse>) => {
      state.occurrences = action.payload.data;
      state.pagination = action.payload.pagination;
      state.favorites = [];
    },
    setFavoriteEvent: (state, action: PayloadAction<Event>) => {
      // Setting event as a favorite into Favorite List
      const index = state.favorites.findIndex(
        event => event.id === action.payload.id,
      );
      if (index !== -1) {
        state.favorites.splice(index, 1);
      } else {
        const newFavoriteEvent = {...action.payload, favorite: true};
        state.favorites.push(newFavoriteEvent);
      }

      // Changing favorite status
      state.events = state.events.map(event => {
        if (event.id === action.payload.id) {
          return {...event, favorite: !event.favorite};
        }
        return event;
      });
      state.occurrences = state.occurrences.map(event => {
        if (event.id === action.payload.id) {
          return {...event, favorite: !event.favorite};
        }
        return event;
      });
    },
    setEventsCalendar: (state, action: PayloadAction<Event>) => {
      state.events = state.events.map(event => {
        if (event.id === action.payload.id) {
          return {...event, schedule: !event.schedule};
        }
        return event;
      });
      state.occurrences = state.occurrences.map(event => {
        if (event.id === action.payload.id) {
          return {...event, schedule: !event.schedule};
        }
        return event;
      });
    },
  },
});

export const {
  setEvents,
  setEventsOccurrences,
  setFavoriteEvent,
  setEventsCalendar,
} = eventSlice.actions;

export const eventSelector = (state: {eventSliceReducer: InitStateProps}) =>
  state.eventSliceReducer.events;

export const eventOccurrenceSelector = (state: {
  eventSliceReducer: InitStateProps;
}) => state.eventSliceReducer.occurrences;

export const eventFavoritesSelector = (state: {
  eventSliceReducer: InitStateProps;
}) => state.eventSliceReducer.favorites;

export default eventSlice.reducer;
