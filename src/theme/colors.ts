import {warning} from '@utilities/logger';

export const palette = {
  //red
  redNormal: '#b40e36',
  redLight: '#cf6a80',

  //white
  whiteNormal: '#FFFFFF',

  //black
  blackNormal: '#727272',
} as const;

/**
 * Tagged string function that return one color from the color palette by using it name. If no color is found it will throw an error
 * @param name Tagged string with one of the colors in the palette with the format colorGroupShadeNumber or just groupShadeNumber ex. colorAlert500, alert500
 * @returns
 */
export function colors(name: keyof typeof palette) {
  const hasError = !(name in palette);

  if (hasError) {
    warning(
      hasError,
      `Requested color named ${name} was not found on theme palette.`,
    );
    throw new Error(
      `Requested color named ${name} was not found on theme palette.`,
    );
  }

  return palette[name];
}
