export interface EventResponse {
  pagination: Pagination;
  data: Event[];
  favorites: Event[];
}

export interface Pagination {
  current_page: number;
  next_url: string;
}

export interface Event {
  id: number;
  title: string;
  image_url: string;
  end_date: string;
  end_at: string;
  location: string;
  description: string;
  short_description: string;
  program_titles: string[];
  favorite: boolean;
  schedule: boolean;
}
