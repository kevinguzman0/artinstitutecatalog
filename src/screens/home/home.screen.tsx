import React, {useState} from 'react';
import CarouselCustom from '@screens/home/components/Carousel/carousel';
import TitleSection from './components/Texts/title-section';
import Header from '@components/Headers/header-custom';
import {
  eventOccurrenceSelector,
  eventSelector,
} from '@store/slices/events.reducer';
import {useAppSelector} from '@hooks/store.hook';
import ItemEventSkeleton from '@components/Skeletons/item-event.skeleton';
import Container from '@components/Containers/container';
import ScrollViewContainer from '@components/Containers/scrollView.container';
import {RefreshControl} from 'react-native';
import {useQueryEvents} from '@hooks/useQueryEvents.hook';

const HomeScreen = () => {
  const [refreshing, setRefreshing] = useState(false);
  const dataEventStore = useAppSelector(eventSelector);
  const dataEventOccurrencesStore = useAppSelector(eventOccurrenceSelector);

  const {isLoading, isFetching, refetchEventOccurrences, refetchEvents} =
    useQueryEvents();

  const onRefresh = () => {
    setRefreshing(true);
    // Takes an array of promises and returns a new promise that only resolves when
    // all the promises in the array have resolved.
    Promise.all([refetchEvents(), refetchEventOccurrences()]).then(() => {
      setRefreshing(false);
    });
  };

  return (
    <Container>
      <Header title={'Explore'} />

      <ScrollViewContainer
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {/* Section 1 */}
        <TitleSection title="Events" />

        {isLoading || isFetching ? (
          <ItemEventSkeleton />
        ) : (
          <CarouselCustom data={dataEventStore || []} />
        )}

        {/* Section 2 */}
        <TitleSection title="Event Occurrences" />

        {isLoading || isFetching ? (
          <ItemEventSkeleton />
        ) : (
          <CarouselCustom data={dataEventOccurrencesStore || []} />
        )}
      </ScrollViewContainer>
    </Container>
  );
};

export default HomeScreen;
