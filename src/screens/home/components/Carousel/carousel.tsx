import {Event} from '@models/events/eventResponse.model';
import {hp, wp} from '@utilities/responsive';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import ItemCarousel from './item';

type CarouselCustomProps = {
  data: Event[];
};

const CarouselCustom = ({data}: CarouselCustomProps) => {
  const [activeIndex, setActiveIndex] = React.useState<number>(0);

  return (
    <View style={styles.container}>
      <Carousel
        layout="stack"
        horizontal={true}
        layoutCardOffset={18}
        data={data}
        renderItem={({item, index}) => (
          <ItemCarousel
            key={index}
            item={item}
            index={index}
            activeIndex={activeIndex}
          />
        )}
        sliderWidth={wp(95)}
        itemWidth={wp(85)}
        hasParallaxImages={true}
        onSnapToItem={index => setActiveIndex(index)}
      />
    </View>
  );
};

export default CarouselCustom;

const styles = StyleSheet.create({
  container: {
    marginTop: hp(1),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: hp(4),
  },
});
