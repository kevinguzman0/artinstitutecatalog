import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
} from 'redux-persist/es/constants';

export const middlewareConfig = {
  immutableCheck: {warnAfter: 200},
  serializableCheck: {
    warnAfter: 200,
    ignoredActions: [
      FLUSH,
      REHYDRATE,
      PAUSE,
      PERSIST,
      PURGE,
      REGISTER,
      'persist/PERSIST',
    ],
  },
};
