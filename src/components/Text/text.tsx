import React from 'react';
import {Text as RNText} from 'react-native';

// import styles from './models/text.styles';

import type {TextProps as RNTextProps, TextStyle} from 'react-native';
import styles from './styles/text.styles';

export type Category = keyof typeof stylesMap;

export type TextProps = {
  /**
   * Text styles bases on design system options (font family, font size, line height). Defaults to p1
   */
  category?: Category;
  /**
   * Text transformation to uppercase
   */
  uppercase?: boolean;
} & Pick<
  RNTextProps,
  | 'style'
  | 'children'
  | 'selectable'
  | 'testID'
  | 'ellipsizeMode'
  | 'lineBreakMode'
  | 'numberOfLines'
  | 'nativeID'
>;

const stylesMap = {
  h1: styles.h1,
  h2: styles.h2,
  h3: styles.h3,
  h4: styles.h4,
  p1: styles.p1,
  p2: styles.p2,
  p3: styles.p3,
  b1: styles.b1,
  b2: styles.b2,
  b3: styles.b3,
  b4: styles.b4,
  b5: styles.b5,
  s: styles.s,
  s1: styles.s1,
  s2: styles.s2,
};

function Text({
  style = {},
  children,
  uppercase = false,
  category,
  testID = 'custom-text',
  ...textProps
}: TextProps) {
  const mappedStyle =
    category && category in stylesMap ? stylesMap[category] : stylesMap.p1;
  const _styles = [styles.base, mappedStyle as TextStyle];

  if (uppercase) {
    _styles.push(styles.uppercase);
  }

  return (
    <RNText testID={testID} {...textProps} style={[..._styles, style]}>
      {children}
    </RNText>
  );
}

export default Text;
