/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {colors} from '@theme/colors';

import {View} from 'react-native';
import Header from '@components/Headers/header-custom';
import {useAppSelector} from '@hooks/store.hook';
import {eventFavoritesSelector} from '@store/slices/events.reducer';

import Item from './components/item-favorites';
import Text from '@components/Text/text';
import Container from '@components/Containers/container';
import ScrollViewContainer from '@components/Containers/scrollView.container';

const FavoriteScreen = () => {
  const favoritesData = useAppSelector(eventFavoritesSelector);
  return (
    <Container>
      <Header title={'Favorites'} goBack={true} />
      <ScrollViewContainer>
        {favoritesData.length > 0 ? (
          favoritesData.map(item => {
            return <Item key={item.id} item={item} />;
          })
        ) : (
          <View style={{alignItems: 'center'}}>
            <Text category="p3" style={{color: colors('redLight')}}>
              No Events Found
            </Text>
          </View>
        )}
      </ScrollViewContainer>
    </Container>
  );
};

export default FavoriteScreen;
