export function parseLiterals(literals: TemplateStringsArray) {
  return literals.join('');
}

export function toKebabCase(name: string) {
  return name.trim().toLowerCase().replace(/\s+/g, '-');
}

export const capitalizeFirstLetter = (str: string) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};
