import {Animated, Easing} from 'react-native';

export const AnimationIcon = (scaleValue: Animated.Value) => {
  Animated.sequence([
    Animated.timing(scaleValue, {
      toValue: 1,
      duration: 300,
      easing: Easing.linear,
      useNativeDriver: true,
    }),
    Animated.timing(scaleValue, {
      toValue: 0,
      duration: 300,
      easing: Easing.linear,
      useNativeDriver: true,
    }),
  ]).start();
};

export const AnimationIconCalendar = (
  scaleValue: Animated.Value,
  isClicked: boolean,
) => {
  Animated.timing(scaleValue, {
    toValue: isClicked ? 1.3 : 1,
    duration: 200,
    useNativeDriver: true,
  }).start();
};

export const AnimationOpacity = (
  fadeAnim: Animated.Value,
  activeIndex: number,
  index: number,
) => {
  Animated.timing(fadeAnim, {
    toValue: activeIndex === index ? 1 : 0,
    duration: 500,
    useNativeDriver: true,
  }).start();
};
