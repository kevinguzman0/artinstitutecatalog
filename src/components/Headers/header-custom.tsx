import React from 'react';
import Text from '@components/Text/text';
import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {colors} from '@theme/colors';
import {useAppSelector} from '@hooks/store.hook';
import {eventFavoritesSelector} from '@store/slices/events.reducer';
import {useNavigation} from '@react-navigation/native';
import {FAVORITES_SCREEN} from '@navigation/routesName';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {MainStackParamList} from '@navigation/main/main.stacks';

type homeScreenProp = NativeStackNavigationProp<typeof MainStackParamList>;
type HeaderProps = {
  title: string;
  goBack?: boolean;
};

const Header = ({title, goBack = false}: HeaderProps) => {
  const navigation = useNavigation<homeScreenProp>();
  const favorites = useAppSelector(eventFavoritesSelector);

  return (
    <View style={styles.container}>
      <Text category="p1">{title}</Text>
      {!goBack ? (
        <>
          <Icon
            name="heart"
            size={26}
            color={colors('redNormal')}
            onPress={() => navigation.navigate(FAVORITES_SCREEN)}
          />
          <View style={styles.containerCount}>
            <Text category="s">{favorites.length}</Text>
          </View>
        </>
      ) : (
        <Icon
          name="arrowleft"
          size={26}
          color={'#000'}
          onPress={() => navigation.goBack()}
        />
      )}
    </View>
  );
};
export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
  },
  containerCount: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
});
