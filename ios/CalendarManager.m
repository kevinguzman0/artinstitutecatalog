//
//  Counter.m
//  ArtInstituteCatalog
//
//  Created by Kevin Guzman on 11/03/24.
//

#import <Foundation/Foundation.h>

#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(CalendarManager,NSObject)

//RCT_EXTERN_METHOD(increment: (RCTResponseSenderBlock)callback) 

RCT_EXTERN_METHOD(addEvent:(NSString *)name location:(NSString *)location date:(nonnull NSNumber *)date callback:(RCTResponseSenderBlock)callback)


@end
