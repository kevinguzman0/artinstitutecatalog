import React from 'react';
import Text from '@components/Text/text';
import {View} from 'react-native';
import {colors} from '@theme/colors';

type TitleSectionProps = {
  title: string;
};

const TitleSection = ({title}: TitleSectionProps) => {
  return (
    <View>
      <Text style={{color: colors('blackNormal')}} category="p3">
        {title}
      </Text>
    </View>
  );
};

export default TitleSection;
