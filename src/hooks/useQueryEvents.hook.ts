import {
  useGetEventsOccurrencesQuery,
  useGetEventsQuery,
} from '@store/apis/events.api';

export const useQueryEvents = () => {
  const {
    isLoading: isLoadingEvents,
    isFetching: isFetchingEvents,
    refetch: refetchEvents,
  } = useGetEventsQuery(10);
  const {
    isLoading: isLoadingOccurrences,
    isFetching: isFetchingEventsOccurrences,
    refetch: refetchEventOccurrences,
  } = useGetEventsOccurrencesQuery(10);

  const isLoading = isLoadingEvents || isLoadingOccurrences;
  const isFetching = isFetchingEvents || isFetchingEventsOccurrences;

  return {isLoading, isFetching, refetchEvents, refetchEventOccurrences};
};
