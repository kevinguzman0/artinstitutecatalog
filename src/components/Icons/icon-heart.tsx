import {colors} from '@theme/colors';
import {AnimationIcon} from '@utilities/animations';
import React, {useRef} from 'react';
import {Animated, StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

type IconProps = {
  favorite: boolean;
  handleFavorite: () => void;
};

const IconHeart = ({favorite, handleFavorite}: IconProps) => {
  const scaleValue = useRef(new Animated.Value(0)).current;

  const heartScale = scaleValue.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [1, 1.3, 1],
  });

  const onHeartPress = () => {
    handleFavorite();
    AnimationIcon(scaleValue);
  };

  return (
    <View style={styles.containerIcon}>
      <Animated.View style={{transform: [{scale: heartScale}]}}>
        <Icon
          name={favorite ? 'heart' : 'hearto'}
          size={24}
          color={colors('redNormal')}
          onPress={onHeartPress}
        />
      </Animated.View>
    </View>
  );
};

export default IconHeart;

const styles = StyleSheet.create({
  containerIcon: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
