import {EVENT_DETAILS_SCREEN} from '@navigation/routesName';

export type ITabBarIcon = {
  focused: boolean;
  color: string;
  size: number;
};

export type MainStackParamList = {
  [EVENT_DETAILS_SCREEN]: {id: number};
};
