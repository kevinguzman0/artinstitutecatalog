module.exports = {
  presets: [
    'module:@react-native/babel-preset',
    ['@babel/preset-env', {targets: {node: 'current'}}],
    '@babel/preset-react',
    '@babel/preset-typescript',
  ],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          '@src': './src',
          '@navigation': './src/navigation',
          '@screens': './src/screens',
          '@store': './src/store',
          '@assets': './src/assets',
          '@adapters': './src/adapters',
          '@components': './src/components',
          '@hooks': './src/hooks',
          '@services': './src/services',
          '@utilities': './src/utilities',
          '@models': './src/models',
          '@theme': './src/theme',
          '@constants': './src/constants',
          '@types': './src/types',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
