import {fontSize} from '@theme/typography';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  base: {
    color: '#000',
  },
  h1: {
    fontSize: fontSize`39`,
    fontWeight: 'bold',
  },
  h2: {
    fontSize: fontSize`39`,
    fontFamily: '400',
  },
  h3: {
    fontSize: fontSize`31`,
    fontWeight: 'bold',
  },
  h4: {
    fontSize: fontSize`31`,
    fontWeight: '400',
  },
  p1: {
    fontSize: fontSize`24`,
    fontWeight: '700',
  },
  p2: {
    fontSize: fontSize`24`,
    fontFamily: '400',
  },
  p3: {
    fontSize: fontSize`18`,
    fontWeight: 'bold',
  },
  b1: {
    fontSize: fontSize`18`,
    fontFamily: '400',
  },
  b2: {
    fontSize: fontSize`16`,
    fontWeight: 'bold',
  },
  b3: {
    fontSize: fontSize`16`,
    fontFamily: '400',
  },
  b4: {
    fontSize: fontSize`13`,
    fontWeight: 'bold',
  },
  b5: {
    fontSize: fontSize`13`,
    fontFamily: '400',
  },
  s: {
    fontSize: fontSize`10`,
    fontWeight: 'bold',
  },
  s1: {
    fontSize: fontSize`10`,
    fontFamily: '400',
  },
  s2: {
    fontSize: fontSize`10`,
    fontFamily: '400',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
});
