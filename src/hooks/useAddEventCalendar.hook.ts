import {Event} from '@models/events/eventResponse.model';
import {setEventsCalendar} from '@store/slices/events.reducer';
import {NativeModules} from 'react-native';
import {useAppDispatch} from './store.hook';

export const useAddEventCalendar = () => {
  const dispatch = useAppDispatch();
  const {CalendarManager} = NativeModules;

  const handleEventCalendar = (event: Event) => {
    const date = event.end_date
      ? new Date(event.end_date)
      : new Date(event.end_at);
    const dateFormat = date.getTime();

    CalendarManager.addEvent(
      event.title, // name
      event.location, // location
      dateFormat, // date
      (error: string, schedule: string) => {
        if (error) {
          console.error(`Error: ${error}`);
        } else {
          dispatch(setEventsCalendar(event));
          console.log(schedule);
        }
      },
    );
  };
  return {handleEventCalendar};
};
