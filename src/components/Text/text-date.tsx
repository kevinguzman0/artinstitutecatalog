import React from 'react';
import {colors} from '@theme/colors';
import Text from './text';

type TextDateProps = {
  date: string;
};

const TextDate = ({date}: TextDateProps) => {
  const formattedEndDate = date ? date.split('T')[0] : '';

  return (
    <Text category="b4" style={{color: colors('redNormal')}}>
      {formattedEndDate}
    </Text>
  );
};

export default TextDate;
