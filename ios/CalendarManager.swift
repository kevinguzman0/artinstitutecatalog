//
//  Counter.swift
//  ArtInstituteCatalog
//
//  Created by Kevin Guzman on 11/03/24.
//

import Foundation
import EventKit

//@objc(Counter)
//class Counter: NSObject{
//  
//  private var count = 0
//  @objc
//  func increment(_ callback: RCTResponseSenderBlock){
//    count += 1
//    callback([count])
//  }
//  
//  @objc
//  static func requiresMainQueueSetup() -> Bool {
//    return true
//  }
//  
//  func constantsToExport() -> [String: Any]!{
//    return ["initialCount": 0]
//  }
//}

@objc(CalendarManager)
class CalendarManager: NSObject {
  @objc(addEvent:location:date:callback:)
  func addEvent(name: String, location: String, date: NSNumber, callback: @escaping RCTResponseSenderBlock) {
    let eventStore = EKEventStore()
    
    // Request access to the calendar
    eventStore.requestAccess(to: .event) { (granted, error) in
      if (granted) && (error == nil) {
        let event = EKEvent(eventStore: eventStore)
        event.title = name
        event.startDate = Date(timeIntervalSince1970: date.doubleValue / 1000) // timestamp in seconds
        event.endDate = event.startDate.addingTimeInterval(60 * 60) // 1 hour long event
        event.location = location
        event.calendar = eventStore.defaultCalendarForNewEvents
        do {
          try eventStore.save(event, span: .thisEvent)
          callback([NSNull(), "Event added"])
        } catch let error as NSError {
          callback([error.description, NSNull()])
        }
      } else {
        callback(["Access to calendar denied", NSNull()])
      }
    }
  }
  
  // Expose this module to the React Native bridge
  @objc static func requiresMainQueueSetup() -> Bool {
    return true
  }
}
