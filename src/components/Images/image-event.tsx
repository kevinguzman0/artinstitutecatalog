import {Event} from '@models/events/eventResponse.model';
import {MainStackParamList} from '@navigation/main/main.stacks';
import {EVENT_DETAILS_SCREEN} from '@navigation/routesName';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {hp, wp} from '@utilities/responsive';
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Image, View} from 'react-native';

type homeScreenProp = NativeStackNavigationProp<typeof MainStackParamList>;
type ImageEventProps = {
  item: Event;
  noPress?: boolean;
};

const ImageEvent = ({item, noPress = false}: ImageEventProps) => {
  const navigation = useNavigation<homeScreenProp>();

  return (
    <View style={styles.container}>
      <TouchableOpacity
        disabled={noPress}
        onPress={() =>
          navigation.navigate(EVENT_DETAILS_SCREEN, {id: item.id})
        }>
        <Image source={{uri: item.image_url}} style={styles.image} />
      </TouchableOpacity>
    </View>
  );
};

export default ImageEvent;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  image: {
    width: wp(85),
    height: hp(30),
    borderRadius: 10,
    marginTop: hp(2),
  },
});
