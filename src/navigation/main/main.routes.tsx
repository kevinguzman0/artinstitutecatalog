import HomeScreen from '@screens/home/home.screen';
import FavoriteScreen from '@screens/favorites/favorites.screen';
import EventDetailsScreen from '@screens/details/event-details.screen';

import {
  EVENT_DETAILS_SCREEN,
  FAVORITES_SCREEN,
  HOME_SCREEN,
} from '../routesName';

export const mainRoutes = [
  {
    name: HOME_SCREEN,
    component: HomeScreen,
    options: {
      title: 'Explore',
      headerShown: false,
    },
  },
  {
    name: FAVORITES_SCREEN,
    component: FavoriteScreen,
    options: {
      title: 'Favorites',
      headerShown: false,
    },
  },
  {
    name: EVENT_DETAILS_SCREEN,
    component: EventDetailsScreen,
    options: {
      title: 'Details',
      headerShown: false,
    },
  },
];
