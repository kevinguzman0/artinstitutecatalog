import {useAddEventCalendar} from '@hooks/useAddEventCalendar.hook';
import {Event} from '@models/events/eventResponse.model';
import {colors} from '@theme/colors';
import {AnimationIconCalendar} from '@utilities/animations';
import React, {useEffect, useRef, useState} from 'react';
import {Animated, StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

type IconProps = {
  item: Event;
};

const IconCalendar = ({item}: IconProps) => {
  const [isClicked, setIsClicked] = useState(false);
  const scaleValue = useRef(new Animated.Value(1)).current;
  const {handleEventCalendar} = useAddEventCalendar();

  const onHeartPress = () => {
    setIsClicked(true);
    setTimeout(() => {
      setIsClicked(false);
      handleEventCalendar(item);
    }, 300);
  };

  useEffect(() => {
    AnimationIconCalendar(scaleValue, isClicked);
  }, [isClicked, scaleValue]);

  return (
    <View style={styles.containerIcon}>
      <Animated.View style={{transform: [{scale: scaleValue}]}}>
        <Icon
          name={item.schedule ? 'calendar-check-o' : 'calendar-plus-o'}
          size={24}
          color={colors('redNormal')}
          onPress={onHeartPress}
          disabled={item.schedule}
        />
      </Animated.View>
    </View>
  );
};

export default IconCalendar;

const styles = StyleSheet.create({
  containerIcon: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
