import {colors} from '@theme/colors';
import React from 'react';
import {Platform, StyleSheet, View} from 'react-native';

type ContainerProps = {
  children: React.ReactNode;
};

const Container = ({children}: ContainerProps) => {
  return <View style={styles.container}>{children}</View>;
};

export default Container;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: colors('whiteNormal'),
    paddingTop: Platform.OS === 'ios' ? 35 : 0,
  },
});
