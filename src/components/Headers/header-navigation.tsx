import {NativeStackNavigationOptions} from '@react-navigation/native-stack';
import {colors} from '@theme/colors';

export const customHeader: NativeStackNavigationOptions = {
  statusBarStyle: 'dark',
  statusBarColor: colors('whiteNormal'),
  statusBarAnimation: 'fade',
  headerBackTitleVisible: false,
  animation: 'slide_from_right',
  animationDuration: 400,
};
