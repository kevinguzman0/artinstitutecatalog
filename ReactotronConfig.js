import Reactotron, {asyncStorage} from 'reactotron-react-native';
import {AsyncStorage} from '@react-native-async-storage/async-storage';

Reactotron.setAsyncStorageHandler(AsyncStorage)
  .configure({
    name: 'ArtInstituteCatalog App',
  })
  .use(asyncStorage())
  .useReactNative({
    devTools: true,
    editor: false,
    errors: {veto: () => false},
    overlay: false,
  }) // add all built-in react native plugins
  .connect(); // let's connect!
