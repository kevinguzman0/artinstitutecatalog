import Header from '@components/Headers/header-custom';
import React from 'react';
import Item from './components/item-details';
import {useAppSelector} from '@hooks/store.hook';
import {
  eventOccurrenceSelector,
  eventSelector,
} from '@store/slices/events.reducer';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {MainStackParamList} from '@navigation/main/main.types';
import {Event} from '@models/events/eventResponse.model';
import Container from '@components/Containers/container';
import ScrollViewContainer from '@components/Containers/scrollView.container';

export type EventDetailsScreenProps = NativeStackScreenProps<
  MainStackParamList,
  'EventDetailsScreen'
>;

const EventDetailsScreen = ({route}: EventDetailsScreenProps) => {
  const {id} = route.params;

  const events = useAppSelector(eventSelector);
  const occurrences = useAppSelector(eventOccurrenceSelector);

  const eventDetails: Event =
    events?.find(item => item.id === id) ||
    occurrences?.find(item => item.id === id) ||
    ({} as Event);

  return (
    <Container>
      <Header title={'Details'} goBack />
      <ScrollViewContainer>
        <Item item={eventDetails} />
      </ScrollViewContainer>
    </Container>
  );
};

export default EventDetailsScreen;
