import {Event, EventResponse} from '@models/events/eventResponse.model';
import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import {setEvents, setEventsOccurrences} from '@store/slices/events.reducer';
import {BASE_URL} from '@utilities/api';

export const eventsApi = createApi({
  reducerPath: 'eventsApi',
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_URL,
  }),
  endpoints: builder => ({
    getEvents: builder.query<EventResponse, number>({
      query: limit => `/events?limit=${limit}`,
      async onQueryStarted(id, {dispatch, queryFulfilled}) {
        try {
          const res = await queryFulfilled;
          dispatch(setEvents(res.data));
        } catch (resError) {
          console.log('resError', resError);
        }
      },
    }),
    getEventsOccurrences: builder.query<EventResponse, number>({
      query: limit => `/event-occurrences?limit=${limit}`,
      async onQueryStarted(id, {dispatch, queryFulfilled}) {
        try {
          const res = await queryFulfilled;
          dispatch(setEventsOccurrences(res.data));
        } catch (resError) {
          console.log('resError', resError);
        }
      },
    }),
    getEventById: builder.query<Event, number>({
      query: id => ({
        url: `/events/${id}`,
        method: 'GET',
      }),
    }),
  }),
});

export const {reducer, middleware, reducerPath, endpoints} = eventsApi;
export const {
  useGetEventsQuery,
  useGetEventsOccurrencesQuery,
  useGetEventByIdQuery,
} = eventsApi;
