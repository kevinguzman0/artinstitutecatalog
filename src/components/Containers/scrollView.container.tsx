import {hp} from '@utilities/responsive';
import React from 'react';
import {
  RefreshControlProps,
  ScrollView,
  ScrollViewProps,
  StyleSheet,
} from 'react-native';

interface ScrollViewContainerProps extends ScrollViewProps {
  footer?: React.ReactNode;
  refreshControl?: React.ReactElement<
    RefreshControlProps,
    string | React.JSXElementConstructor<any>
  >;
}

const ScrollViewContainer = ({
  children,
  refreshControl,
}: ScrollViewContainerProps) => {
  return (
    <ScrollView
      refreshControl={refreshControl}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.container}>
      {children}
    </ScrollView>
  );
};

export default ScrollViewContainer;

const styles = StyleSheet.create({
  container: {
    paddingBottom: hp(6),
    marginTop: hp(4),
  },
});
