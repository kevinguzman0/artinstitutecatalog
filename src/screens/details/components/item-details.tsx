/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {colors} from '@theme/colors';
import {hp} from '@utilities/responsive';
import {StyleSheet, View} from 'react-native';
import Text from '@components/Text/text';
import {Event} from '@models/events/eventResponse.model';
import ImageEvent from '@components/Images/image-event';
import ReadMore from '@fawazahmed/react-native-read-more';
import IconCalendar from '@components/Icons/icon-calendar';
import TextDate from '@components/Text/text-date';

type ItemProps = {
  item: Event;
};

const Item = ({item}: ItemProps) => {
  return (
    <View>
      <ImageEvent item={item} noPress={true} />

      <View style={styles.containerDetails}>
        <View style={{marginBottom: hp(1), flexDirection: 'row'}}>
          <View style={{flex: 0.8}}>
            <TextDate date={item.end_date || item.end_at} />
            <Text
              category="b5"
              style={{color: colors('redLight'), marginTop: 5}}>
              {item.location}
            </Text>
          </View>
          <IconCalendar item={item} />
        </View>

        <Text category="p3" style={{marginTop: 5}}>
          {item.title}
        </Text>

        <ReadMore
          numberOfLines={5}
          style={styles.description}
          seeMoreStyle={{
            color: colors('redNormal'),
          }}>
          <Text category="b5" style={styles.description}>
            {item.description}
          </Text>
        </ReadMore>

        {item.program_titles && item.program_titles.length > 0 && (
          <View style={{marginTop: hp(1)}}>
            <Text category="b2" style={{color: colors('redLight')}}>
              Program Titles
            </Text>
            {item.program_titles?.map((title, index) => (
              <View key={index} style={{paddingLeft: 10}}>
                <Text
                  key={index}
                  category="b5"
                  style={{marginTop: 5, color: colors('blackNormal')}}>
                  {index + 1}. {title}
                </Text>
              </View>
            ))}
          </View>
        )}
      </View>
    </View>
  );
};

export default Item;

const styles = StyleSheet.create({
  containerDetails: {
    marginTop: hp(1),
    paddingHorizontal: 10,
  },
  description: {
    marginTop: 5,
    textAlign: 'justify',
    color: colors('blackNormal'),
  },
});
