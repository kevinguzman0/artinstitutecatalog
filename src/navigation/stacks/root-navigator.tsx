import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import MainStacks from '@navigation/main/main.stacks';

const RootNavigator = () => {
  return (
    <NavigationContainer>
      <MainStacks />
    </NavigationContainer>
  );
};

export default RootNavigator;
