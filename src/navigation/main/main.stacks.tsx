import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {HOME_SCREEN} from '@navigation/routesName';
import {mainRoutes} from './main.routes';
import {customHeader} from '@components/Headers/header-navigation';
import {ParamListBase} from '@react-navigation/native';

// import {CustomTab} from '@components/navigation/CustomTab';

const Stack = createNativeStackNavigator();

export const MainStackParamList = mainRoutes.reduce((acc, route) => {
  acc[route.name] = undefined;
  return acc;
}, {} as ParamListBase);

const MainStacks = () => {
  return (
    <Stack.Navigator
      initialRouteName={HOME_SCREEN}
      screenOptions={customHeader}>
      {mainRoutes.map(route => (
        <Stack.Screen
          key={route.name}
          name={route.name}
          component={route.component}
          options={route.options}
        />
      ))}
    </Stack.Navigator>
  );
};

export default MainStacks;
